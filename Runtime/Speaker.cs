﻿using Cysharp.Threading.Tasks;
using System;
using System.Threading;
using UnityEngine;

namespace A27.SFX
{
    [Serializable]
    public class Speaker
    {
        public Speaker(Transform target)
        {
            try
            {
                if (target.TryGetComponent(out audioSource) == false)
                {
                    audioSource = target.gameObject.AddComponent<AudioSource>();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            audioSource.priority = 0;
            audioSource.volume = UserData.Global.BgmVol;
            audioSource.playOnAwake = false;
            audioSource.loop = true;
            audioSource.spatialBlend = 0;
        }

        private readonly AudioSource audioSource;
        private CancellationTokenSource token = new CancellationTokenSource();
        private float fadeOutPrograss = 0f;

        public bool IsPlaying => audioSource.isPlaying;

        public Speaker SetVolume(float a_volume)
        {
            CancelAsync();
            audioSource.volume = a_volume;
            return this;
        }
        public async UniTask Play(AudioClip a_clip)
        {
            CancelAsync();
            await FadeoutAsync(a_clip, token.Token);

            audioSource.clip = a_clip;
            audioSource.volume = UserData.Global.BgmVol;
            audioSource.PlayDelayed(SystemData.Global.DelayTime);
        }
        public Speaker PlayOneShot(AudioClip a_clip, float a_volume)
        {
            audioSource.PlayOneShot(a_clip, a_volume);
            return this;
        }
        public Speaker Stop()
        {
            CancelAsync();
            audioSource.Stop();
            return this;
        }
        private void CancelAsync()
        {
            if (!IsPlaying) return;

            token.Cancel();
            token.Dispose();
            token = new CancellationTokenSource();
        }
        private async UniTask FadeoutAsync(AudioClip a_clip, CancellationToken a_token)
        {
            if (!IsPlaying) return;

            float maxVol = audioSource.volume;
            fadeOutPrograss = SystemData.Global.FadeOutTime;
            while (fadeOutPrograss > 0)
            {
                fadeOutPrograss -= Time.deltaTime;
                float newVol = maxVol * fadeOutPrograss / SystemData.Global.FadeOutTime;
                audioSource.volume = newVol;
                await UniTask.NextFrame();
            }
        }
    }
}
