﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace A27.SFX
{
    [Serializable]
    public class SystemData
    {
        public float FadeOutTime { get => fadeOutTime; }
        [SerializeField]
        private float fadeOutTime = 1.0f;

        public float DelayTime { get => delayTime; }
        [SerializeField]
        private float delayTime = 0.5f;


        public void Save()
        {
            string data = JsonUtility.ToJson(this);
            File.WriteAllText(Path, data);
            Debug.Log("Save System setting!");

            Global.Load();
        }

        public void Load()
        {
            using (UnityWebRequest request = UnityWebRequest.Get(Path))
            {
                UnityWebRequestAsyncOperation rao = request.SendWebRequest();

                string log = "Load System setting!\n<color=grey>";
                int wait = 0;
                while (!rao.isDone)
                {
                    wait++;
                    log += wait + "：" + rao.progress + "\n";
                }
                log += "</color>download finish：" + wait + "\n\n";

                if (request.result == UnityWebRequest.Result.Success)
                {
                    string data = request.downloadHandler.text;
                    SystemData work = JsonUtility.FromJson<SystemData>(data);

                    fadeOutTime = work.fadeOutTime;
                    delayTime = work.delayTime;

                    Debug.Log(log);
                }
                else Debug.LogError(request.error);
            }
        }


        public static string Path
        {
            get
            {
#if UNITY_EDITOR
                string path = Application.streamingAssetsPath + "/A27/SFX";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                path += "/System.json";
                if (!File.Exists(path))
                {
                    StreamWriter sw = File.CreateText(path);
                    sw.Write(JsonUtility.ToJson(new SystemData()));
                    sw.Close();
                    UnityEditor.AssetDatabase.Refresh();
                }
                return path;
#else
            return Application.streamingAssetsPath + "/A27/SFX/System.json";
#endif
            }
        }

        public static SystemData Global
        {
            get
            {
                if (global == null)
                {
                    global = new SystemData();
                    global.Load();
                }
                return global;
            }
        }
        private static SystemData global;

    }
}