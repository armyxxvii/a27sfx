﻿using A27.EnumDic;
using System;
using UnityEngine;

namespace A27.SFX
{
    [Serializable]
    public class SFX
    {
        [EnumDic(nameof(EDic.SFX_ButtonActType))]
        public string ButtonActType;
        public AudioClip data;
    }
}
