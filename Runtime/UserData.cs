﻿using System;
using System.IO;
using UnityEngine;

namespace A27.SFX
{
    [Serializable]
    public class UserData
    {
        [Range(0f, 1f)] public float BgmVol = 0.8f;
        [Range(0f, 1f)] public float SfxVol = 0.9f;

        public void Save()
        {
            string data = JsonUtility.ToJson(this);
            File.WriteAllText(Path, data);

            Debug.Log("Save user setting!");
            Global.Load();
        }

        public void Load()
        {
            string data = File.ReadAllText(Path);
            UserData work = JsonUtility.FromJson<UserData>(data);

            BgmVol = work.BgmVol;
            SfxVol = work.SfxVol;

            Debug.Log("Load user setting!");
        }


        public static string Path
        {
            get
            {
#if UNITY_EDITOR
                string path = Application.persistentDataPath + "/A27/SFX";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                path += "/User.json";
                if (!File.Exists(path))
                {
                    StreamWriter sw = File.CreateText(path);
                    sw.Write(JsonUtility.ToJson(new UserData()));
                    sw.Close();
                    UnityEditor.AssetDatabase.Refresh();
                }
                return path;
#else
            return Application.persistentDataPath + "/A27/SFX/User.json";
#endif
            }
        }

        public static UserData Global
        {
            get
            {
                if (global == null)
                {
                    global = new UserData();
                    global.Load();
                }
                return global;
            }
        }
        private static UserData global;
    }
}