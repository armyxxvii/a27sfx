using A27.EnumDic;
using Cysharp.Threading.Tasks;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace A27.SFX
{
    [RequireComponent(typeof(AudioSource))]
    public class BgmManager : MonoBehaviour
    {
        string currentSceneType;
        string currentBGMType;

        Speaker speaker;
        [SerializeField] BGM[] bgms;
        [SerializeField] SFX[] sfxs;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                speaker = new Speaker(transform);
                OnUserChanged.AddListener(ResetVolume);
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void ResetVolume()
        {
            speaker = speaker.SetVolume(UserData.Global.BgmVol);
            Debug.Log("UserSettingChanged");
        }
        private AudioClip GetBGM(string a_sceneType, string a_bgmType)
        {
            BGM b = Array.Find(bgms, x => x.SceneType == a_sceneType && x.BGMType == a_bgmType);
            return b?.data;
        }
        private AudioClip GetSFX(string a_buttonActType)
        {
            SFX s = Array.Find(sfxs, x => x.ButtonActType == a_buttonActType);
            return s?.data;
        }
        private bool CheckBGM(string a_sceneType, string a_bgmType, out AudioClip data)
        {
            if (speaker.IsPlaying && a_sceneType == currentSceneType && a_bgmType == currentBGMType)
            {
                Debug.LogWarning("Same BGM...");
                data = null;
                return false;
            }

            data = GetBGM(a_sceneType, a_bgmType);
            if (data == null)
            {
                Debug.LogWarning($"Bgm[{EDic.SFX_SceneType.GetName(a_sceneType)} , {EDic.SFX_BGMType.GetName(a_bgmType)}] not found...");
                return false;
            }
            return true;
        }

        private async UniTask PlayBGMAsync(string a_sceneType, string a_bgmType)
        {
            if (CheckBGM(a_sceneType, a_bgmType, out AudioClip data) == false) return;

            await speaker.Play(data);

            currentSceneType = a_sceneType;
            currentBGMType = a_bgmType;
            Debug.Log($"PlayBgm[{EDic.SFX_SceneType.GetName(a_sceneType)} , {EDic.SFX_BGMType.GetName(a_bgmType)}]");
        }


        #region static Function
        static BgmManager Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType<BgmManager>();
                if (instance == null)
                    instance = new GameObject("Bgm Manager").AddComponent<BgmManager>();
                return instance;
            }
        }
        static BgmManager instance = null;

        #region Events
        public static UnityEvent OnUserChanged = new UnityEvent();

        public static void UserSettingChanged()
            => OnUserChanged?.Invoke();
        #endregion

        public static void PlayBGM(string a_sceneType, string a_bgmType)
            => Instance.PlayBGMAsync(a_sceneType, a_bgmType).Forget();
        public static void StopBGM()
            => Instance.speaker.Stop();
        public static void ButtonSFX(string a_buttonActType)
        {
            AudioClip data = Instance.GetSFX(a_buttonActType);
            if (data != null)
                Instance.speaker.PlayOneShot(data, UserData.Global.SfxVol);
            else
                Debug.LogWarning($"SFX[{EDic.SFX_ButtonActType.GetName(a_buttonActType)}] not found...");
        }
        #endregion
    }
}
