﻿using A27.EnumDic;
using System;
using UnityEngine;

namespace A27.SFX
{
    [Serializable]
    public class BGM
    {
        [EnumDic(nameof(EDic.SFX_SceneType))]
        public string SceneType;
        [EnumDic(nameof(EDic.SFX_BGMType))]
        public string BGMType;
        public AudioClip data;
    }
}
