﻿using A27.EnumDic;
using System;
using UnityEditor;
using UnityEngine;
using static A27.EnumDic.EDic;
using static A27.SFX.BgmManager;

namespace A27.SFX
{
    [CustomEditor(typeof(BgmManager))]
    public class BgmManagerInspector : Editor
    {
        static int m_stIndex = 1, m_bgmIndex = 0;
        static EDic m_sceneTypes, m_bgmTypes, m_sfxTypes;
        static string m_sceneType = "", m_bgmType = "";
        static bool sfxFold = true, bgmFold = true;

        private void OnEnable()
        {
            m_sceneTypes = Array.Find(Dics, x => x.Name == nameof(SFX_SceneType));
            m_bgmTypes = Array.Find(Dics, x => x.Name == nameof(SFX_BGMType));
            m_sfxTypes = Array.Find(Dics, x => x.Name == nameof(SFX_ButtonActType));

            m_sceneType = m_sceneTypes.Guids[m_stIndex];
            m_bgmType = m_bgmTypes.Guids[m_bgmIndex];
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (!Application.isPlaying) return;

            bgmFold = EditorGUILayout.Foldout(bgmFold, "BGM Test");
            if (bgmFold)
            {
                using (new EditorGUILayout.HorizontalScope("helpbox"))
                {
                    using (var c = new EditorGUI.ChangeCheckScope())
                    {
                        m_stIndex = EditorGUILayout.Popup(m_stIndex, m_sceneTypes.Names);
                        if (c.changed) m_sceneType = m_sceneTypes.Guids[m_stIndex];
                    }
                    using (var c = new EditorGUI.ChangeCheckScope())
                    {
                        m_bgmIndex = EditorGUILayout.Popup(m_bgmIndex, m_bgmTypes.Names);
                        if (c.changed) m_bgmType = m_bgmTypes.Guids[m_bgmIndex];
                    }
                    if (GUILayout.Button("Play", GUILayout.Width(40)))
                        PlayBGM(m_sceneType, m_bgmType);
                    if (GUILayout.Button("Stop", GUILayout.Width(40)))
                        StopBGM();
                }
            }
            sfxFold = EditorGUILayout.Foldout(sfxFold, "ButtonSFX Test");
            if (sfxFold)
            {
                using (new EditorGUILayout.VerticalScope("helpbox"))
                {
                    for (int i = 0; i < m_sfxTypes.Length; i++)
                    {
                        if (GUILayout.Button(m_sfxTypes.Names[i]))
                            ButtonSFX(m_sfxTypes.Guids[i]);
                    }
                }
            }
        }
    }
}
