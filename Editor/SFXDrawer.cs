﻿using UnityEditor;
using UnityEngine;

namespace A27.SFX
{
    [CustomPropertyDrawer(typeof(SFX))]
    public class SFXDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Rect r = EditorGUI.IndentedRect(position);

            float w = Mathf.Max(r.width * 0.25f, 70f);
            float w2 = r.width - w;
            r.height = EditorGUIUtility.singleLineHeight;
            r.width = w;
            EditorGUI.PropertyField(r, property.FindPropertyRelative("ButtonActType"), GUIContent.none);
            r.x += w;
            r.width = w2;
            EditorGUI.PropertyField(r, property.FindPropertyRelative("data"), GUIContent.none);
        }
    }
}