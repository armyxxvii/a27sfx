using UnityEditor;

namespace A27.SFX
{
    public static class MenuItems
    {
        [MenuItem("Tools/A27/SFX/Add BgmManager")]
        public static void AddLoader()
        {
        }

        [MenuItem("Tools/A27/SFX/Edit System Setting &1", priority = 2000)]
        public static void EditSystem()
        {
            EditSystem_Window.ShowWindow();
        }

        [MenuItem("Tools/A27/SFX/Edit User Setting &2", priority = 2000)]
        public static void EditUser()
        {
            EditUser_Window.ShowWindow();
        }
    }
}