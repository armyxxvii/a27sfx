﻿using UnityEditor;
using UnityEngine;

namespace A27.SFX
{
    [CustomPropertyDrawer(typeof(BGM))]
    public class BGMDrawer : PropertyDrawer
    {
        const float space = 0f;
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label) * 2 + space;
        }
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Rect r = EditorGUI.IndentedRect(position);

            float w = r.width * 0.5f;
            float w2 = r.width;

            r.height = EditorGUIUtility.singleLineHeight;
            r.width = w;
            EditorGUI.PropertyField(r, property.FindPropertyRelative("SceneType"), GUIContent.none);
            r.x += w;
            EditorGUI.PropertyField(r, property.FindPropertyRelative("BGMType"), GUIContent.none);
            r.x -= w;
            r.y += r.height;
            r.width = w2;
            EditorGUI.PropertyField(r, property.FindPropertyRelative("data"), GUIContent.none);
        }
    }
}